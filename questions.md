> When on gitolite, the “admin” user is the one that can create repos, so when a user goes away, access is closed but repo ownership is never changed.
> 
> On github.com each user is an owner and it has full control over the repo, I’m not sure how is this different in the version we run, do we create repos with an admin account? If not, and say pedro.white creates a repo for a super cool app and then pedro walks away, what happens to the repo if we need to disable his account? Who is the owner? I’m not sure if there is a best practice for this or how are we doing it.

A repo on github will have different levels of access which are __Owners__, __admin__, __write__, and __read__. 

* Owners can do anything
* Admins can do anything except create and delete any repo in the organization. They also cannot pull(read), push(write), or fork(copy).
* Writers can push, pull, fork, pull request, merge and close pull requests, open and close issues, edit any comments. 
* Readers can pull, fork, send pull requests, open issues, edit their own comments.  

Ligers are owners of the liger organization but may only have write or even read access to other organizations and repositories. 

You can create repositories in an organization that you have Owner, Admin, or write permissions to. You can always create repositories in your personal space. 
If a user creates an organization or repo and leaves the company, a github admin can spoof that user's login and become that user for the purpose of gaining access to the repo or organization. 

Since github uses active directory, any user who has been terminated should have their AD creds revoked. Which in turn means that they would not be able to gain access to github.



------

> ...how we might benefit from using it(github) in our own daily work.

Two words: "version control"

Imagine working on a github document and putting a lot of time into it but then something catastrophic happens and you lose all of your work. That would suck. Let's not suck. 
Github gives you the entire history of the document you were working on. Everytime you commit the file and push to your repo you create history. You could go back 20 edits and see what changed. This is true of any file you decide to store in github.

Another benifit is being able to easily share your work with people in your organization. You could ask someone to pull down your repository and work on it with you.

------

> Can you please talk about github gists? 
> 
> It seems pretty easy to use it but can you explain the scope of gist:
> When to use a gist vs creating a new repo
> Can we share code and document stuff? If so can we combine it with confluence / jira?
> Can we pull the gist snippets into a confluence page?
> How easy is to search for a gist(s) on a specific topic
> Show/Explain other users of gists

Gists can be used in the same way as a repository. You can fork, clone, publish, tag, pull, etc. I have only ever used Gists to share sensitive data with other people or to post the output of log files and code that I wanted to share outside of my repository. Gists are a quick way to share info with someone. I wouldn't suggest using them as a replacement for a repository but as a way to keep track of notes. 

You can imbed Gists into confluence docs by using the URL if confluence has the *Gister* plugin installed. 
You can imbed Gists into Jira by linking the url to the gist. If your gist is private you have to provide the url to the people you want to share with. 

Every gist is saved under your gist user profile. For example, mine is located at: __http://gist.github.body.prod/mark-white__
Searching through gists is easy: __http://gist.github.body.prod/search__

 
